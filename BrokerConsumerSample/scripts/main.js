﻿
$(document).ready(function () {
    
    Broker = function () {
        return {
            send_cmd: function (cmd) {

                $.ajax({
                    url: '/index/execute',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        cmd: cmd
                    },
                    success: function (response) {
                        try{
                            response = JSON.parse(response);
                            if ("msgResp" in response) {
                                $('#output').html(response.msgResp.replaceAll('\n','<br />'));
                            } else {
                                $('#output').text(response.msg);
                            }
                        } catch (err) {
                            alert("Ops... deu ruim!");
                            console.error(err);
                        }

                    },
                    error: function (e, x, t) {
                        console.log(e, x, t)
                    }
                });
            }
        }
    }

    window.broker = new Broker();

    $('.command-action').on('click', function () {
        var id = $(this).data('id');
        var value = $(id).data('command') + " '" + $(id).val() + "'";
        broker.send_cmd(value);
    });

    $('.command-button').on('click', function () {
        var value = $(this).data('command');
        broker.send_cmd(value);

    });


    $('.command-select').on('change', function () {
        
        var command = $(this).data("command");
        var value = command + " '" + $(this).val() + "'";
        if (value == "") return false;

        broker.send_cmd(value)
    });

    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };



    

});