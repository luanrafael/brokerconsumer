var arvore = [

	{
	    name: 'Z - Interface',
	    trigger: 'tab1'
	},
	{
	    name: 'X - Pesquisa',
	    trigger: 'tab3'
	},
	{
	    name: 'C - ERP',
	    trigger: 'focar'
	},
	{
	    name: 'V - Histórico',
	    trigger: "alerta '$HISTORICO'"
	},
	{
	    name: 'B - Outros',
	    trigger: [

			{
			    name: '1 - Planilhas',
			    trigger: [
					{
					    name: '1 - Prospecção 2016',
					    trigger: "abreArquivo 'C:\\DOCS CM\\Banco de Dados\\Prospecção 2016.xls'"
					},
					{
					    name: '2 - Comparação Legislação',
					    trigger: "abreArquivo 'C:\\DOCS CM\\ANS\\Comparação da Legislação.xlsx'"
					},
					{
					    name: '3 - RN',
					    trigger: "abreArquivo 'C:\\DOCS CM\\ANS\\RN 388.xlsx'"
					},
					{
					    name: '4 - Drive',
					    trigger: "ieabreh 'https://drive.google.com/open?id=1Yu4WmjMJDtWLrioWP2y44o_C2zLPMLT97xbibfYcN38' "
					},
			    ]
			},
			{
			    name: '2 - Propostas',
			    trigger: [
					{
					    name: '1 - Proposta Comercial Pelna Saude',
					    trigger: "abreArquivo 'C:\\DOCS CM\\Plena Saude\\Proposta Comercial Plena Saude - 2016 03.docx' "
					},
					{
					    name: '2 - Modelo de Proposta',
					    trigger: "abreArquivo 'C:\\DOCS CM\\Modelos de Documentos\\Modelo de Proposta Comercial.docx'"
					},
			    ]
			},
			{
			    name: '3 - PPTS',
			    trigger: [
					{
					    name: '1 - M2G Soluções de Atendimento',
					    trigger: "abreArquivo 'C:\\DOCS CM\\Apresentacoes\\M2G Solucoes de Atendimento para Operadora de Saude.pptx'"
					},
					{
					    name: '2 - Apresentação online',
					    trigger: "ieabreh 'http://www.integracaom2g.com.br/WebForm/webformm2gcomercial/ &' "
					},
			    ]
			},
			{
			    name: '4 - Programas',
			    trigger: [
					{
					    name: '1 - Skype',
					    trigger: 'start `skype &`'
					},
					{
					    name: '3 - Google Drive',
					    trigger: "ieabreh 'https://drive.google.com/drive/?tab=mo&rfd=1#' 'Google Drive' handlerGoogleDrive"
					},
					{
					    name: '4 - Bizage',
					    trigger: "alerta 'bizage'"
					},
			    ]
			},
			{
			    name: '5 - Email',
			    trigger: [
					{
					    name: '1 - E-mail - M2G',
					    trigger: "ieabreh 'https://mail.google.com' 'Email' handlerGmail"
					}
			    ]
			}

	    ]
	},

]

window.onload = function () {


    var li = '<li data-trigger="{{trigger}}" class="action" data-img="{{img}}" data-shortchut="{{s}}"> <span class="key"> {{i}} </span> <a href="#"> {{name}} </a></li>';
    var submenu = '<li data-trigger="submenu" data-img="{{img}}" class="submenu" data-shortchut="{{s}}"> <i class="material-icons"></i> <span class="key"> {{i}} </span> <a href="#"> {{name}}  </a>';

    var menu = Arv.create(arvore, '<ul>');
    document.getElementById('arv').innerHTML = menu;

    bind();

}



function bind() {

    var actions = document.querySelectorAll('.action');


    for (var i = 0; i < actions.length; i++) {
        var action = actions[i];
        action.onclick = function (e) {
            send_cmd(this.getAttribute('data-trigger'));
        }
    };


    Mousetrap.bind('f7', function () {

        reset();

        $('#arv-modal').modal('show');

        var menus = document.querySelector('#arv ul').children || [];

        for (var i = 0; i < menus.length; i++) {
            var m = menus[i];
            m.className += m.className.indexOf('active') > -1 ? '' : ' active';
        }

    });

    var alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

    Mousetrap.bind(alpha, function (e, keycode) {

        if (document.querySelector('.active')) {
            var ul = document.querySelector('.active').parentNode;
            var actives = document.querySelectorAll('.active')
            var selector = '.active[data-shortchut="' + keycode.trim().toUpperCase() + '"]';
            var active = ul.querySelector(selector);

            if (!active) return;

            if (active.getAttribute('data-trigger') != 'submenu') {
                reset();
                return send_cmd(active.getAttribute('data-trigger'));
            }

            for (var i = actives.length - 1; i >= 0; i--)
                actives[i].className = actives[i].className.replace('active', '').trim();

            active.className += active.className.indexOf('open') > -1 ? '' : ' open';

            var children = active.querySelector('ul').children || [];

            for (var i = children.length - 1; i >= 0; i--) {
                children[i].className += children[i].className.indexOf('active') > -1 ? '' : ' active';
            }

        }

    });

    Mousetrap.bind('esc', function (e) {

        var opens = document.querySelectorAll('.open');

        if (!opens || opens.length <= 0) {
            reset();
            $('#arv-modal').modal('hide');
            return;
        }

        var lastOpen = opens[opens.length - 1];
        lastOpen.className = lastOpen.className.replace('open', '').replace('active', '').trim();
        var ul = lastOpen.querySelector('ul').children || [];

        for (var i = 0; i < ul.length; i++) {
            var o = ul[i];
            o.className = o.className.replace('open', '').trim();
            o.className = o.className.replace('active', '').trim();
        }

        var children = lastOpen.parentNode.children || [];

        for (var i = children.length - 1; i >= 0; i--) {
            children[i].className += children[i].className.indexOf('active') > -1 ? '' : ' active';
        }

    });

    var liAction = document.querySelectorAll('.action');

    for (var i = 0; i < liAction.length; i++) {

        liAction[i].addEventListener('click', function (e) {
            reset();
        });
    }
}


function send_cmd(cmd) {
    window.broker.send_cmd(cmd);
    $('#arv-modal').modal('hide');
}

function reset() {
    var opens = document.querySelectorAll('.open,.active') || [];

    for (var i = 0; i < opens.length; i++) {
        var o = opens[i];
        o.className = o.className.replace('open', '').trim();
        o.className = o.className.replace('active', '').trim();
    }
}



function gerar(m, n) {
    var alfa = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
    m = m > alfa.length ? alfa.length : m;
    n = n > alfa.length ? alfa.length : n;
    var arv = [];
    for (var i = 0; i < m; i++) {
        var arvI = {
            name: multL(alfa[i], 1)
        }
        var triggers = []
        for (var ii = 0; ii < n; ii++) {

            triggers.push(
				{
				    name: multL(alfa[i], ii + 2),
				    trigger: multL(alfa[i], ii + 2)
				}
			);
        }
        arvI.trigger = triggers;
        arv.push(arvI);
    }
    return arv;
}

function multL(s, l) {
    var ss = '';
    for (var i = 0; i < l; i++)
        ss += s
    return ss;
}