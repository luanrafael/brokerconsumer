﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Net;

namespace BrokerConsumerSample.Services
{
    /**
    * Classe responsável por enviar comandos ao broker
    ***/
    public class Execution
    {

        public string execute(string cmd, string key)
        {
            return send_command("http://broker.kanban360.com.br:8090/cloud", cmd, key, "100");
        }

        public string send_command(string host, string cmd, string key, string timeout)
        {

            string dest = "CLI-" + key;
            string rmte = "WEB-" + key;
            string msg_env = cmd;
            string consumidor = "Avulso";
            string msg_ret = "false";

            return post(host, rmte, consumidor, dest, msg_env, timeout, msg_ret);


        }

        public string post(string url, string prmte, string pconsumer, string pdest, string penv, string ptimeout, string pret)
        {
            string result = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Accept = "application/json";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            
            String rmte, consumer, dest, env, resp, ret;

            rmte = HttpUtility.UrlEncode("" + prmte);
            consumer = HttpUtility.UrlEncode("" + pconsumer);
            dest = HttpUtility.UrlEncode("" + pdest);
            env = HttpUtility.UrlEncode("" + penv);
            resp = HttpUtility.UrlEncode("" + ptimeout);//ALTERAR PARA TIMOUT "ptimeout"
            ret = HttpUtility.UrlEncode("" + pret);

            String parsedContent = 
            "idRmte=" + rmte + "&" +
            "idConsumidor=" + consumer + "&" +
            "idDest=" + dest + "&" +
            "msgEnv=" + env + "&" +
            "msgResp=10&" +
            "msgRet=";

            Console.WriteLine(parsedContent);

            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] dados = encoding.GetBytes(parsedContent);


            using (Stream newStream = request.GetRequestStream())
            {
                newStream.Write(dados, 0, dados.Length);
            }

            try
            {
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return result;
        }

    }
}