﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrokerConsumerSample.Controllers
{
    public class Filter : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            Controller controller = filterContext.Controller as Controller;

            if (session["user"] == null)
            {
                
                controller.HttpContext.Response.Redirect("/Index/Login?next=" + filterContext.HttpContext.Request.Url.AbsoluteUri);
            }
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // The action filter logic.
        }


    }
}