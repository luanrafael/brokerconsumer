﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerConsumerSample.Services;

namespace BrokerConsumerSample.Controllers
{
    public class IndexController : Controller
    {
        // GET: Index
        [Filter]
        public ActionResult Index()
        {
            ViewData["err"] = Request.Params["err"];
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Filter]
        public ActionResult Execute(string cmd)
        {
            Execution service = new Execution();

            if(Session["fingerprint"] == null)
            {
                
                return Json("{\"err\": true, \"msg\": \"Antes de enviar algum comando é precisso registrar sua máquina!\" }");
            }

            string fingerprint = Session["fingerprint"] as string;

            string result = service.execute(cmd, fingerprint);

            return Json(result);
        }

        [Filter]
        public ActionResult Register(string fingerprint)
        {
            Session.Add("fingerprint", fingerprint);
            ViewData["fingerprint"] = fingerprint;
            return View();
        }


        
        public ActionResult Login(string username, string password)
        {
            if(Request.HttpMethod == "POST")
            {
                if (username.Equals("admin") && password.Equals("admin"))
                {
                    string next = Request.Params.Get("next");
                    Session.Add("user", "admin");
                    return Redirect(next == null || next == "" ? "/" : next);
                }

                ViewData["err"] = "Usuário ou senha inválidos";

            }

            return View();
        }
    }
}